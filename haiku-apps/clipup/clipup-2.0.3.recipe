SUMMARY="An app to keep your latest copies"

DESCRIPTION="
	Ever had something important in your clipboard but you copied something \
	else? ClipUp allows you to save multiple clips, and can be shown by \
	pressing option+space or clicking on the icon. It can save your clipboard content \
	even when rebooting the computer!
	" 

HOMEPAGE="https://github.com/HaikuArchives/ClipUp"
SRC_URI="git://github.com/HaikuArchives/ClipUp.git#a9218f7c0148d818969105e84d3d5e176f580521"
REVISION="1"

LICENSE="MIT"
COPYRIGHT="2002 Werner Freytag"

ARCHITECTURES="x86_gcc2"


PROVIDES="
	clipup = $portVersion
	app:clipup = $portVersion
	"

REQUIRES="
	haiku >= $haikuVersion
	"

BUILD_PREREQUIRES="
	makefile_engine
	cmd:make
	cmd:mkdepend
	cmd:gcc
	cmd:xres
	"
BUILD_REQUIRES="
	haiku_devel >= $haikuVersion
	"

BUILD()
{
	cd trunk
	cd "Input Device"
	make
	cd "../Input Filter"
	make
	cd "../Main"
	make
	cd ..
}

INSTALL()
{
	cd trunk
	mkdir -p $addOnsDir/input_server/devices
	mkdir -p $addOnsDir/input_server/filters

	cd "Input Device"
	make install INSTALL_DIR=temp
	mv temp/* $addOnsDir/input_server/devices
	cd ..
	
	cd "Input Filter"
	make install INSTALL_DIR=temp
	mv temp/* $addOnsDir/input_server/filters
	cd ..
	
	cd "Main"
	make install INSTALL_DIR=$appsDir
	mkdir -p "$dataDir/deskbar/menu/Desktop applets"
	symlinkRelative -s "$appsDir/ClipUp" "$dataDir/deskbar/menu/Desktop applets"
	
}
